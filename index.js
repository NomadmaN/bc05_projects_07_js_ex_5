// Bài 1: Quản lý tuyển sinh

// đặt biến khu vực và đối tượng
const VUNG_CAO = "vungCao";
const VUNG_XA = "vungXa";
const VUNG_KHO_KHAN = "vungKhoKhan";
const NGHEO_BEN_VUNG = "ngheoBenVung";
const MO_COI = "moCoi";
const KHUYET_TAT = "khuyetTat";
// hàm áp dụng điểm ưu tiên theo khu vực
function heSoUuTienTheoKhuVuc(uutien) {
  var diemUuTien;
  if (uutien == VUNG_CAO) {
    diemUuTien = 2;
  } else if (uutien == VUNG_XA) {
    diemUuTien = 1;
  } else if (uutien == VUNG_KHO_KHAN) {
    diemUuTien = 0.5;
  } else {
    diemUuTien = 0;
  }
  return diemUuTien;
}

// hàm áp dụng điểm ưu tiên theo đối tượng
function heSoUuTienTheoDoiTuong(uutien) {
  if (uutien == NGHEO_BEN_VUNG) {
    diemUuTien = 2.5;
  } else if (uutien == MO_COI) {
    diemUuTien = 1.5;
  } else if (uutien == KHUYET_TAT) {
    diemUuTien = 1;
  } else {
    diemUuTien = 0;
  }
  return diemUuTien;
}
// Hàm chính ra kết quả
function btnKetQuaB1() {
  var diemMon1 = document.getElementById("diemMon1").value * 1;
  var diemMon2 = document.getElementById("diemMon2").value * 1;
  var diemMon3 = document.getElementById("diemMon3").value * 1;
  var diemChuan = document.getElementById("diemChuan").value * 1;
  var khuVuc = document.getElementById("khuVuc").value;
  var doiTuong = document.getElementById("doiTuong").value;
  var diemUuTienKhuVuc = heSoUuTienTheoKhuVuc(khuVuc);
  var diemUuTienDoiTuong = heSoUuTienTheoDoiTuong(doiTuong);
  var diemTong = 0;
  var diemTongGomUuTien = 0;
  diemTong = diemMon1 + diemMon2 + diemMon3;
  diemTongGomUuTien = diemTong + diemUuTienKhuVuc + diemUuTienDoiTuong;

  if (diemTongGomUuTien < diemChuan) {
    document.getElementById(
      "showKetQuaB1"
    ).innerHTML = `Bạn đã rớt. Tổng điểm là ${diemTongGomUuTien} thấp hơn điểm chuẩn yêu cầu`;
  } else {
    document.getElementById(
      "showKetQuaB1"
    ).innerHTML = `Chúc mừng, bạn đã đậu. Tổng điểm là ${diemTongGomUuTien} (điểm chính: ${diemTong} + điểm ưu tiên: ${
      diemUuTienKhuVuc + diemUuTienDoiTuong
    })`;
  }
}

// Bài 2: Tính tiền điện

// tạo hàm tính tiền điện theo Kw tiêu thụ
function tienDienTheoKWTieuThu(tieuthu) {
  var tienSoKWTieuThu;
  if (tieuthu <= 50) {
    tienSoKWTieuThu = 500 * tieuthu;
  } else if (tieuthu <= 100) {
    tienSoKWTieuThu = 650 * (tieuthu - 50) + 500 * 50;
  } else if (tieuthu <= 200) {
    tienSoKWTieuThu = 850 * (tieuthu - 100) + 650 * 50 + 500 * 50;
  } else if (tieuthu <= 350) {
    tienSoKWTieuThu = 1100 * (tieuthu - 200) + 850 * 100 + 650 * 50 + 500 * 50;
  } else {
    tienSoKWTieuThu =
      1300 * (tieuthu - 350) + 1100 * 150 + 850 * 100 + 650 * 50 + 500 * 50;
  }
  return tienSoKWTieuThu;
}
// hàm chính
function btnKetQuaB2() {
  var hoTen = document.getElementById("hoTen").value;
  var soKW = document.getElementById("soKW").value;
  var tienDienPhaiTra = tienDienTheoKWTieuThu(soKW);
  document.getElementById(
    "showKetQuaB2"
  ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${tienDienPhaiTra.toLocaleString()} VND`;
}


